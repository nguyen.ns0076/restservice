package com.restservice.services.assampler;

import com.restservice.controllers.BacklogController;
import com.restservice.model.Backlog;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BacklogModelAssempler implements RepresentationModelAssembler<Backlog, EntityModel<Backlog>> {

    @Override
    public EntityModel<Backlog> toModel(Backlog backlog) {
        return EntityModel.of(backlog,
                linkTo(methodOn(BacklogController.class).one(backlog.getBacklogID())).withSelfRel(),
                linkTo(methodOn(BacklogController.class).all()).withRel("backlogs"));
    }
}