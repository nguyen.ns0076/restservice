package com.restservice.services;

import com.restservice.model.User;
import com.restservice.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {
//    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
//
//    @Bean
//    CommandLineRunner initDatabase(EmployeeRepository repository) {
//        return args -> {
//            log.info("Preloading " + repository.save(new Employee("Bilbo Baggins", "burglar")));
//            log.info("Preloading " + repository.save(new Employee("Frodo Baggins", "thief")));
//        };
//    }
//
//    @Bean
//    CommandLineRunner initUserDatabase(UserRepository repository) {
//        return args -> {
//            log.info("Preloading " + repository.save(new User("Bilbo Baggins", "burglar", "123")));
//        };
//    }
}
