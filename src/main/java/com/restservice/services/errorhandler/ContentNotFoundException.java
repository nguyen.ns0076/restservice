package com.restservice.services.errorhandler;

public class ContentNotFoundException extends RuntimeException {
    public ContentNotFoundException(Long id, String content){ super("Couldn't find "+ content + " with id " + id);}
}
