package com.restservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Backlog {

    public @Id @GeneratedValue Long BacklogID;

    public String ProjectIdentifier;

    Backlog() {}

    Backlog(String ProjectIdentifier) {
        this.ProjectIdentifier = ProjectIdentifier;
    }

    public Long getBacklogID() {
        return BacklogID;
    }

    public String getProjectIdentifier() {
        return ProjectIdentifier;
    }

    public void setBacklogID(Long backlogID) {
        BacklogID = backlogID;
    }

    public void setProjectIdentifier(String projectIdentifier) {
        ProjectIdentifier = projectIdentifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Backlog backlog = (Backlog) o;
        return Objects.equals(BacklogID, backlog.BacklogID) &&
                Objects.equals(ProjectIdentifier, backlog.ProjectIdentifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(BacklogID, ProjectIdentifier);
    }

    @Override
    public String toString() {
        return "Backlog{" +
                "BacklogID=" + BacklogID +
                ", ProjectIdentifier='" + ProjectIdentifier + '\'' +
                '}';
    }
}
