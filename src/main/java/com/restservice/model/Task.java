package com.restservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class Task {
    public @Id
    @GeneratedValue
    Long TaskID;

    public Long BacklogID;

    public String ProjectTaskSequence;

    public String Summary;

    public String AcceptanceCriteria;

    public String Status;

    public String Priority;

    public Date DueDate;

    public Date CreatedDate;

    public Date UpdateDate;

    Task() {}

    public Task(Long BacklogID, String ProjectTaskSequence, String Summary, String AcceptanceCriteria,
         String Status, String Priority, Date DueDate, Date CreatedDate, Date UpdateDate) {
        this.BacklogID = BacklogID;
        this.ProjectTaskSequence = ProjectTaskSequence;
        this.Summary = Summary;
        this.AcceptanceCriteria = AcceptanceCriteria;
        this.Status = Status;
        this.Priority = Priority;
        this.DueDate = DueDate;
        this.CreatedDate = CreatedDate;
        this.UpdateDate = UpdateDate;
    }

    public Long getTaskID() {
        return TaskID;
    }

    public Long getBacklogID() {
        return BacklogID;
    }

    public String getProjectTaskSequence() {
        return ProjectTaskSequence;
    }

    public String getSummary() {
        return Summary;
    }

    public String getAcceptanceCriteria() {
        return AcceptanceCriteria;
    }

    public String getStatus() {
        return Status;
    }

    public String getPriority() {
        return Priority;
    }

    public Date getDueDate() {
        return DueDate;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public Date getUpdateDate() {
        return UpdateDate;
    }

    public void setTaskID(Long taskID) {
        TaskID = taskID;
    }

    public void setBacklogID(Long backlogID) {
        BacklogID = backlogID;
    }

    public void setProjectTaskSequence(String projectTaskSequence) {
        ProjectTaskSequence = projectTaskSequence;
    }

    public void setSummary(String summary) {
        Summary = summary;
    }

    public void setAcceptanceCriteria(String acceptanceCriteria) {
        AcceptanceCriteria = acceptanceCriteria;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public void setDueDate(Date dueDate) {
        DueDate = dueDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }

    public void setUpdateDate(Date updateDate) {
        UpdateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(TaskID, task.TaskID) &&
                Objects.equals(BacklogID, task.BacklogID) &&
                Objects.equals(ProjectTaskSequence, task.ProjectTaskSequence) &&
                Objects.equals(Summary, task.Summary) &&
                Objects.equals(AcceptanceCriteria, task.AcceptanceCriteria) &&
                Objects.equals(Status, task.Status) &&
                Objects.equals(Priority, task.Priority) &&
                Objects.equals(DueDate, task.DueDate) &&
                Objects.equals(CreatedDate, task.CreatedDate) &&
                Objects.equals(UpdateDate, task.UpdateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(TaskID, BacklogID, ProjectTaskSequence, Summary, AcceptanceCriteria, Status, Priority, DueDate, CreatedDate, UpdateDate);
    }

    @Override
    public String toString() {
        return "Task{" +
                "TaskID=" + TaskID +
                ", BacklogID=" + BacklogID +
                ", ProjectTaskSequence='" + ProjectTaskSequence + '\'' +
                ", Summary='" + Summary + '\'' +
                ", AcceptanceCriteria='" + AcceptanceCriteria + '\'' +
                ", Status='" + Status + '\'' +
                ", Priority='" + Priority + '\'' +
                ", DueDate=" + DueDate +
                ", CreatedDate=" + CreatedDate +
                ", UpdateDate=" + UpdateDate +
                '}';
    }
}
