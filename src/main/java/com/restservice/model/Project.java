package com.restservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class Project {

    public @Id @GeneratedValue Long Id;

    public String ProjectName;

    public String ProjectIdentifier;

    public String Description;

    public Date StartDate;

    public Date EndDate;

    public Date CreatedAt;

    public Date UpdateAt;

    Project() {}

    Project(String ProjectName, String ProjectIdentifier, String Description,
            Date StartDate, Date EndDate, Date CreatedAt, Date UpdateAt) {
        this.ProjectName = ProjectName;
        this.ProjectIdentifier = ProjectIdentifier;
        this.Description = Description;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.CreatedAt = CreatedAt;
        this.UpdateAt = UpdateAt;
    }

    public Long getId() {
        return Id;
    }

    public String getProjectName() {
        return ProjectName;
    }

    public String getProjectIdentifier() {
        return ProjectIdentifier;
    }

    public String getDescription() {
        return Description;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public Date getUpdateAt() {
        return UpdateAt;
    }

    public void setId(Long id) {
        Id = id;
    }

    public void setProjectName(String projectName) {
        ProjectName = projectName;
    }

    public void setProjectIdentifier(String projectIdentifier) {
        ProjectIdentifier = projectIdentifier;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setStartDate(Date startDate) {
        StartDate = startDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public void setCreatedAt(Date createdAt) {
        CreatedAt = createdAt;
    }

    public void setUpdateAt(Date updateAt) {
        UpdateAt = updateAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(Id, project.Id) &&
                Objects.equals(ProjectName, project.ProjectName) &&
                Objects.equals(ProjectIdentifier, project.ProjectIdentifier) &&
                Objects.equals(Description, project.Description) &&
                Objects.equals(StartDate, project.StartDate) &&
                Objects.equals(EndDate, project.EndDate) &&
                Objects.equals(CreatedAt, project.CreatedAt) &&
                Objects.equals(UpdateAt, project.UpdateAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, ProjectName, ProjectIdentifier, Description, StartDate, EndDate, CreatedAt, UpdateAt);
    }

    @Override
    public String toString() {
        return "Project{" +
                "Id=" + Id +
                ", ProjectName='" + ProjectName + '\'' +
                ", ProjectIdentifier='" + ProjectIdentifier + '\'' +
                ", Description='" + Description + '\'' +
                ", StartDate=" + StartDate +
                ", EndDate=" + EndDate +
                ", CreatedAt=" + CreatedAt +
                ", UpdateAt=" + UpdateAt +
                '}';
    }
}
