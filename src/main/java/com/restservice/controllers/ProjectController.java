package com.restservice.controllers;

import com.restservice.model.Project;
import com.restservice.repository.ProjectRepository;;
import com.restservice.services.assampler.ProjectModelAssempler;
import com.restservice.services.errorhandler.ContentNotFoundException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ProjectController {

    private final ProjectRepository repository;
    private final ProjectModelAssempler assempler;

    ProjectController(ProjectRepository repository, ProjectModelAssempler assempler) {
        this.repository = repository;
        this.assempler = assempler;
    }

    @GetMapping("/projects")
    public CollectionModel<EntityModel<Project>> all() {
        List<EntityModel<Project>> project = repository.findAll().stream()
                .map(assempler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(project,
                linkTo(methodOn(ProjectController.class).all()).withSelfRel());
    }

    @GetMapping("/projects/{id}")
    public EntityModel<Project> one(@PathVariable Long id) {
        Project project = repository.findById(id)
                .orElseThrow(() -> new ContentNotFoundException(id, "Project"));

        return assempler.toModel(project);
    }

    @PostMapping("/projects")
    public ResponseEntity<?> newProject(@RequestBody Project newProject) {
        EntityModel<Project> entityModel = assempler.toModel(repository.save(newProject));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @PutMapping("/projects/{id}")
    Project replaceProject(@RequestBody Project newProject, @PathVariable Long id) {

        return repository.findById(id)
                .map(project -> {
                    project.setProjectName(newProject.getProjectName());
                    project.setProjectIdentifier(newProject.getProjectIdentifier());
                    project.setDescription(newProject.getDescription());
                    project.setStartDate(newProject.getStartDate());
                    project.setEndDate(newProject.getEndDate());
                    project.setCreatedAt(newProject.getCreatedAt());
                    project.setUpdateAt(newProject.getUpdateAt());
                    return repository.save(project);
                })
                .orElseGet(() -> {
                    newProject.setId(id);
                    return repository.save(newProject);
                });
    }

    @DeleteMapping("/projects/{id}")
    void deleteProject(@PathVariable Long id) {
        repository.deleteById(id);
    }
}