package com.restservice.controllers;

import com.restservice.model.Backlog;
import com.restservice.repository.BacklogRepository;
import com.restservice.services.assampler.BacklogModelAssempler;
import com.restservice.services.errorhandler.ContentNotFoundException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class BacklogController {

    private final BacklogRepository repository;
    private final BacklogModelAssempler assempler;

    BacklogController(BacklogRepository repository, BacklogModelAssempler assempler) {
        this.repository = repository;
        this.assempler = assempler;
    }

    @GetMapping("/backlogs")
    public CollectionModel<EntityModel<Backlog>> all() {
        List<EntityModel<Backlog>> backlog = repository.findAll().stream()
                .map(assempler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(backlog,
                linkTo(methodOn(BacklogController.class).all()).withSelfRel());
    }

    @GetMapping("/backlogs/{id}")
    public EntityModel<Backlog> one(@PathVariable Long id) {
        Backlog backlog = repository.findById(id)
                .orElseThrow(() -> new ContentNotFoundException(id, "Backlog"));

        return assempler.toModel(backlog);
    }

    @PostMapping("/backlogs")
    public ResponseEntity<?> newBacklog(@RequestBody Backlog newBacklog) {
        EntityModel<Backlog> entityModel = assempler.toModel(repository.save(newBacklog));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @PutMapping("/backlogs/{id}")
    Backlog replaceBacklog(@RequestBody Backlog newBacklog, @PathVariable Long id) {

        return repository.findById(id)
                .map(backlog -> {
                    backlog.setProjectIdentifier(newBacklog.getProjectIdentifier());
                    return repository.save(backlog);
                })
                .orElseGet(() -> {
                    newBacklog.setBacklogID(id);
                    return repository.save(newBacklog);
                });
    }

    @DeleteMapping("/backlogs/{id}")
    void deleteBacklog(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
