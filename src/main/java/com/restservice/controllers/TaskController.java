package com.restservice.controllers;

import com.restservice.model.Task;
import com.restservice.repository.TaskRepository;
import com.restservice.services.assampler.TaskModelAssempler;
import com.restservice.services.errorhandler.ContentNotFoundException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class TaskController {

    private final TaskRepository repository;
    private final TaskModelAssempler assempler;

    TaskController(TaskRepository repository, TaskModelAssempler assempler) {
        this.repository = repository;
        this.assempler = assempler;
    }

    @GetMapping("/tasks")
    public CollectionModel<EntityModel<Task>> all() {
        List<EntityModel<Task>> task = repository.findAll().stream()
                .map(assempler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(task,
                linkTo(methodOn(UserController.class).all()).withSelfRel());
    }

    @GetMapping("/tasks/{id}")
    public EntityModel<Task> one(@PathVariable Long id) {
        Task task = repository.findById(id)
                .orElseThrow(() -> new ContentNotFoundException(id, "Task"));

        return assempler.toModel(task);
    }

    @PostMapping("/tasks")
    public ResponseEntity<?> newTask(@RequestBody Task newTask) {
        EntityModel<Task> entityModel = assempler.toModel(repository.save(newTask));

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @PutMapping("/tasks/{id}")
    Task replaceTask(@RequestBody Task newTask, @PathVariable Long id) {

        return repository.findById(id)
                .map(task -> {
                    task.setBacklogID(newTask.getBacklogID());
                    task.setProjectTaskSequence(newTask.getProjectTaskSequence());
                    task.setSummary(newTask.getSummary());
                    task.setAcceptanceCriteria(newTask.getAcceptanceCriteria());
                    task.setStatus(newTask.getStatus());
                    task.setPriority(newTask.getPriority());
                    task.setDueDate(newTask.getDueDate());
                    task.setCreatedDate(newTask.getCreatedDate());
                    task.setUpdateDate(newTask.getUpdateDate());
                    return repository.save(task);
                })
                .orElseGet(() -> {
                    newTask.setTaskID(id);
                    return repository.save(newTask);
                });
    }

    @DeleteMapping("/tasks/{id}")
    void deleteTask(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
