# Rest Service API 

Build with Java, Spring Boot, HATEOAS, H2 and MySQL

---

## Structure Project

### src.main.java.com.restservice 
- controllers: User/Task/Project/Backlog (Controller)
- model: User/Task/Project/Backlog
- repository: User/Task/Project/Backlog (Repository)
- service: assampler, errorhandler
### Main: main class application

---

## Config Database
### create database name rest_service in MySQL
```sh
mysql> create database rest_service;
mysql> create user 'springuser'@'%' identified by 'ThePassword';
mysql> grant all on rest_service.* to 'springuser'@'%';
```

### config application.properties

```
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/rest_service
spring.datasource.username=springuser
spring.datasource.password=ThePassword
```



